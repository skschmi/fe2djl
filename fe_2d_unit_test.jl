using PyPlot
include("./fe_2d.jl")

## TODO: Test "Grid1D" and associated 1D functions

all_success = true

#********************************
#**** BuildD Test ***************
#********************************

# BuildD Test 1
D = BuildD(1, 0)
D_correct = [1 0 0; 0 1 0; 0 0 0.5]
if( abs(det(D-D_correct)) >= 1e-9 )
    print("BuildD Test 1 FAILED\n")
    print("D = \n",D,"\n")
    print("D_correct = \n",D_correct,"\n")
    all_success = false
else
    print("BuildD Test 1 SUCCESS\n")
end

# BuildD Test 2
D = BuildD( 10000000.0, 0.3 )
D_correct = [13461538.46 5769230.769 0; 5769230.769 13461538.46 0; 0 0 3846153.846]
if( abs(det(D-D_correct)) >= 1e-9 )
    print("BuildD Test 2 FAILED\n")
    print("D = \n",D,"\n")
    print("D_correct = \n",D_correct,"\n")
    all_success = false
else
    print("BuildD Test 1 SUCCESS\n")
end


#********************************
#**** LagrangeNodes Test ********
#********************************

# LagrangeNodes Test 1
result = LagrangeNodes(1, 0, 1, 1, 1, 0, 1, 1)
if( result != [0 0; 1 0; 0 1; 1 1]' )
    print("LagrangeNodes Test 1 FAILED\n")
    println(result)
    all_success = false
else
    print("LagrangeNodes Test 1 SUCCESS\n")
end

# LagrangeNodes Test 2
result = LagrangeNodes(1, 0, 1, 2, 1, 0, 1, 2)
if( result != [0 0;  0.5 0;  1 0;  0 0.5;  0.5 0.5;  1 0.5;  0 1;  0.5 1;  1 1]' )
    print("LagrangeNodes Test 2 FAILED\n")
    println( result )
    all_success = false
else
    print("LagrangeNodes Test 2 SUCCESS\n")
end

# LagrangeNodes Test 3
result = LagrangeNodes(2, 0, 1, 1, 2, 0, 1, 1)
if( result != [0 0; 0.5 0; 1 0; 0 0.5; 0.5 0.5; 1 0.5; 0 1; 0.5 1; 1 1]' )
    print("LagrangeNodes Test 3 FAILED\n")
    println(result)
    all_success = false
else
    print("LagrangeNodes Test 3 SUCCESS\n")
end



#********************************
#**** LagrangeIEN Test **********
#********************************

# LagrangeIEN Test 1
if( LagrangeIEN(1,1,2,2) != [1,2,3,4,5,6,7,8,9] )
    print("LagrangeIEN Test 1 FAILED\n")
    all_success = false
else
    print("LagrangeIEN Test 1 SUCCESS\n")
end

# LagrangeIEN Test 2
if( LagrangeIEN(1,1,1,1) != [1,2,3,4] )
    print("LagrangeIEN Test 2 FAILED\n")
    all_success = false
else
    print("LagrangeIEN Test 2 SUCCESS\n")
end

# LagrangeIEN Test 3
B = zeros(Int64,4,4)
for e=1:4
    B[:,e] = LagrangeIEN(e,2,1,1)
end
if( B != [1 2 4 5; 2 3 5 6; 4 5 7 8; 5 6 8 9]' )
    print("LagrangeIEN Test 3 FAILED\n")
    print("Incorrect B = \n")
    print(B,"\n")
    all_success = false
else
    print("LagrangeIEN Test 3 SUCCESS\n")
end

#7 8 9
#4 5 6
#1 2 3


#**
#*****
#***********
#****************
#*************************
#**** Grid2D Test **********
#*************************
#****************
#***********
#*****
#**


#************
### GRID 1
#************

# Specify the displacement constraint
displacementConstraint =    
            function(grid::Grid2D,e,a,dofi)
                return false,0.0
            end

# Specify the traction on each element
tractionForElement =    
            function(self::Grid2D,e)
                T = zeros(2,(self.p+1)*(self.q+1))
                return T
            end

# m: Number of elements (x-direction)
# M: Width of x-direction
# Mmin: Start x-value of the geometry
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio
grid = Grid2D(1,0.,1.,1,
            1,0.,1.,1,
            1.,1.,displacementConstraint,tractionForElement)

#println( "Jacobian(grid,1,0.2,0.3)")
J,DNDXI_DNDETA = Jacobian(grid,1,0.2,0.3)
if( J != [0.5 0; 0 0.5] )
    println( "Jacobian Test 1 FAILED" )
    println( "J = ",J)
    all_success = false
else
    println( "Jacobian Test 1 SUCCESS" )
end

#println( "DelBasisRealSpace(grid,1,0.2,0.3)" )
DNDX_DNDY = DelBasisRealSpace(grid,1,0.2,0.3)
if( DNDX_DNDY != [-0.35 0.35 -0.65 0.65; -0.4 -0.6 0.4 0.6] )
    println( "DelBasisRealSpace Test 1 FAILED" )
    println( "DNDX_DNDY = ",DNDX_DNDY)
    all_success = false
else
    println( "DelBasisRealSpace Test 1 SUCCESS" )
end

#println( "ParamToReal(grid,1,0.2,0.3)")
x,y = ParamToReal(grid,1,0.2,0.3)
if( (x,y) != (0.6,0.65) )
    println( "ParamToReal Test 1 FAILED" )
    all_success = false
else
    println( "ParamToReal Test 1 SUCCESS" )
end

#println("(1) Did the ElementIntegral function work?")
integral = ElementIntegral(grid,1)
if( integral != [0.24999999999999986,0.24999999999999986,0.2499999999999999,0.2499999999999999] )
    println( "ElementIntegral Test 1 FAILED" )
    all_success = false
else
    println( "ElementIntegral Test 1 SUCCESS" )
end


#println("(1) Did the ElementFaceIntegrals function work?")
faceintegral = ElementFaceIntegrals(grid,1)
if( faceintegral != [0.5 0.0 0.5 0.0;
                     0.0 0.5 0.5 0.0;
                     0.5 0.0 0.0 0.5;
                     0.0 0.5 0.0 0.5] )
    println( "ElementFaceIntegrals Test 1 FAILED" )
    all_success = false
else
    println( "ElementFaceIntegrals Test 1 SUCCESS" )
end

#************
### GRID 2
#************

# m: Number of elements (x-direction)
# M: Width of x-direction
# Mmin: Start x-value of the geometry
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio
grid = Grid2D(2,0.,1.,1,
            2,0.,1.,1,
            1.,1.,displacementConstraint,tractionForElement)

J,DNDXI_DNDETA = Jacobian(grid,1,-0.7746, -0.7746)
det_J = det(J)
if( det_J != 0.0625 )
    println( "Jacobian Test 2 FAILED" )
    println( "det(J) = ",det_J)
    all_success = false
else
    println( "Jacobian Test 2 SUCCESS" )
end

ANS = DelBasisRealSpace(grid,1,-0.7746, -0.7746)
if( ANS != [-1.7746 1.7746 -0.22540000000000004 0.22540000000000004;
            -1.7746 -0.22540000000000004 1.7746 0.22540000000000004] )
    println( "DelBasisRealSpace Test 2 FAILED" )
    println( "ANS = ",ANS)
    all_success = false
else
    println( "DelBasisRealSpace Test 2 SUCCESS" )
end

x,y = ParamToReal(grid,1,-0.7746, -0.7746)
if( (x,y) != (0.05635000000000001,0.05635000000000001) )
    println( "ParamToReal Test 2 FAILED" )
    println( "x,y = ",(x,y))
    all_success = false
else
    println( "ParamToReal Test 2 SUCCESS" )
end



#println("(2) Did the ElementIntegral function work?")
integral = ElementIntegral(grid,1)
if( integral != [0.062499999999999965,0.062499999999999965,0.06249999999999997,0.06249999999999997] )
    println( "ElementIntegral Test 2 FAILED" )
    all_success = false
else
    println( "ElementIntegral Test 2 SUCCESS" )
end


#println("(2) Did the ElementFaceIntegrals function work?")
faceintegral = ElementFaceIntegrals(grid,1)
if( faceintegral != [0.25 0.0 0.25 0.0;
                     0.0 0.25 0.25 0.0;
                     0.25 0.0 0.0 0.25;
                     0.0 0.25 0.0 0.25] )
    println( "ElementFaceIntegrals Test 2 FAILED" )
    all_success = false
else
    println( "ElementFaceIntegrals Test 2 SUCCESS" )
end


#Did the GetB function work?
B = GetB(grid,1,-0.7746, -0.7746)
if( B[:,:,1] != [-1.7746 0.0; 0.0 -1.7746; -1.7746 -1.7746] )
    println( "GetB Test 1 FAILED" )
    println(B[:,:,1])
    all_success = false
else
    println( "GetB Test 1 SUCCESS" )
end





#************
### GRID 3
#************

# m: Number of elements (x-direction)
# M: Width of x-direction
# Mmin: Start x-value of the geometry
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio  
grid = Grid2D(2,0.,1.,2,
            2,0.,1.,2,
            1.,1.,displacementConstraint,tractionForElement)    

J,DNDXI_DNDETA = Jacobian(grid,1,-0.7746, -0.7746)
det_J = det(J)
if( det_J != 0.06249999999999999 )
    println( "Jacobian Test 3 FAILED" )
    println( "det(J) = ",det_J)
    all_success = false
else
    println( "Jacobian Test 3 SUCCESS" )
end

ANS = DelBasisRealSpace(grid,1,-0.7746, -0.7746)
if( ANS != [-3.504143473872 4.259076627743999 -0.7549331538719999 -2.039333692256 2.4786880245120004 -0.439354332256 0.44507716612800013 -0.5409646522560001 0.095887486128;
            -3.5041434738720008 -2.039333692256001 0.44507716612800025 4.259076627744 2.478688024512001 -0.5409646522560002 -0.7549331538720001 -0.43935433225600007 0.09588748612800002] )
    println( "DelBasisRealSpace Test 3 FAILED" )
    println( "ANS = ",ANS)
    all_success = false
else
    println( "DelBasisRealSpace Test 3 SUCCESS" )
end

x,y = ParamToReal(grid,1,-0.7746, -0.7746)
if( (x,y) != (0.056350000000000025,0.056350000000000004) )
    println( "ParamToReal Test 3 FAILED" )
    println( "x,y = ",(x,y))
    all_success = false
else
    println( "ParamToReal Test 3 SUCCESS" )
end


#println("(3) Did the ElementIntegral function work?")
integral = ElementIntegral(grid,1)
if( integral != [0.0069444444444444415,0.027777777777777773,0.006944444444444444,0.027777777777777773,0.11111111111111105,0.02777777777777778,0.006944444444444446,0.02777777777777778,0.006944444444444446] )
    println( "ElementIntegral Test 3 FAILED" )
    all_success = false
else
    println( "ElementIntegral Test 3 SUCCESS" )
end


#println("(3) Did the ElementFaceIntegrals function work?")
faceintegral = ElementFaceIntegrals(grid,1)
if( faceintegral != [0.0833333333333333 0.0 0.0833333333333333 0.0;
                     0.0 0.0 0.33333333333333315 0.0;
                     0.0 0.0833333333333333 0.08333333333333334 0.0;
                     0.33333333333333315 0.0 0.0 0.0;
                     0.0 0.0 0.0 0.0;
                     0.0 0.33333333333333315 0.0 0.0;
                     0.08333333333333334 0.0 0.0 0.0833333333333333;
                     0.0 0.0 0.0 0.33333333333333315;
                     0.0 0.08333333333333334 0.0 0.08333333333333334] )
    println( "ElementFaceIntegrals Test 3 FAILED" )
    all_success = false
else
    println( "ElementFaceIntegrals Test 3 SUCCESS" )
end


#Did the GetB function work?
B = GetB(grid,1,-0.7746, -0.7746)
if( B[:,:,2] != [4.259076627743999 0.0; 0.0 -2.039333692256001; -2.039333692256001 4.259076627743999] )
    println( "GetB Test 2 FAILED" )
    println(B[:,:,2])
    all_success = false
else
    println( "GetB Test 2 SUCCESS" )
end


#************
### GRID 4
#************

# m: Number of elements (x-direction)
# M: Width of x-direction
# Mmin: Start x-value of the geometry
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio
grid = Grid2D(1,0.,1.,1,
            1,0.,1.,1,
            1.,1.,displacementConstraint,tractionForElement)
grid.nodes[1,3] = 0.2
grid.nodes[2,3] = 1.
grid.nodes[1,4] = 0.8
grid.nodes[2,4] = 1.

#println("(4) Did the ElementIntegral function work?")
integral = ElementIntegral(grid,1)
if( integral != [0.21666666666666656,0.21666666666666656,0.18333333333333326,0.18333333333333324] )
    println( "ElementIntegral Test 4 FAILED" )
    all_success = false
else
    println( "ElementIntegral Test 4 SUCCESS" )
end

#println("(4) Did the ElementFaceIntegrals function work?")
faceintegral = ElementFaceIntegrals(grid,1)
if( faceintegral != [0.5099019513592785 0.0 0.5 0.0;
                     0.0 0.5099019513592785 0.5 0.0;
                     0.5099019513592785 0.0 0.0 0.30000000000000004;
                     0.0 0.5099019513592785 0.0 0.30000000000000004] )
    println( "ElementFaceIntegrals Test 4 FAILED" )
    all_success = false
else
    println( "ElementFaceIntegrals Test 4 SUCCESS" )
end


#*****
#*******
#*** Gauss Quadrature Tests ***
#*******
#*****

GQTable = GaussQuadTable(NumGaussPts(4),NumGaussPts(4))

pts_correct =
    [-0.7745966692414834   0.0                 0.7745966692414834 -0.7745966692414834 0.0 0.7745966692414834 -0.7745966692414834  0.0                0.7745966692414834
     -0.7745966692414834  -0.7745966692414834 -0.7745966692414834  0.0                0.0 0.0                 0.7745966692414834  0.7745966692414834 0.7745966692414834]

wts_correct =
    [0.308641975308642,
    0.49382716049382697,
    0.308641975308642,
    0.49382716049382697,
    0.7901234567901227,
    0.49382716049382697,
    0.308641975308642,
    0.49382716049382697,
    0.308641975308642]

if( GQTable.pts != pts_correct )
    println( "GaussQuadTable Points Test 1 FAILED" )
    println( "GQTable.pts = ",GQTable.pts )
    all_success = false
else
    println( "GaussQuadTable Points Test 1 SUCCESS" )
end

if( GQTable.wts != wts_correct )
    println( "GaussQuadTable Weights Test 1 FAILED" )
    println( "GQTable.wts = ",GQTable.pts )
    all_success = false
else
    println( "GaussQuadTable Weights Test 1 SUCCESS" )
end



#************
### GRID 5
#************

displacementConstraint =    function(grid::Grid2D,e,a,dofi)
                                if(dofi == 1 && grid.nodes[dofi,grid.IEN[a,e]] == grid.M)  
                                    # Far right edge has displacement BC of 1.0
                                    return true,1.0
                                elseif(grid.nodes[1,grid.IEN[a,e]] == 0.0)
                                    # Far left edge has displacement BC of 0.0 in both x and y directions.
                                    return true,0.0
                                end
                                return false,0.0
                            end

# m: Number of elements (x-direction)
# M: Width of x-direction
# Mmin: Start x-value of the geometry
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio
grid = Grid2D(1,0.,1.,1,
            1,0.,1.,1,
            1.,0.,displacementConstraint,tractionForElement)

# Unit testing for AssembleElementK
K_e = AssembleElementK(grid,1)

if( K_e != [0.4999999999999998 0.12499999999999993 -0.2499999999999999 -0.12499999999999993 -4.163336342344337e-17 0.12499999999999994 -0.24999999999999983 -0.12499999999999994;
            0.12499999999999993 0.4999999999999998 0.12499999999999993 -4.163336342344337e-17 -0.12499999999999993 -0.2499999999999999 -0.12499999999999993 -0.24999999999999983;
            -0.2499999999999999 0.12499999999999993 0.4999999999999998 -0.12499999999999993 -0.24999999999999983 0.12499999999999994 -4.163336342344337e-17 -0.12499999999999994;
            -0.12499999999999993 -4.163336342344337e-17 -0.12499999999999993 0.4999999999999998 0.12499999999999993 -0.24999999999999983 0.12499999999999993 -0.2499999999999999;
            -4.163336342344337e-17 -0.12499999999999993 -0.24999999999999983 0.12499999999999993 0.4999999999999998 -0.12499999999999994 -0.2499999999999999 0.12499999999999994;
            0.12499999999999994 -0.2499999999999999 0.12499999999999994 -0.24999999999999983 -0.12499999999999994 0.4999999999999998 -0.12499999999999994 -4.163336342344337e-17;
            -0.24999999999999983 -0.12499999999999993 -4.163336342344337e-17 0.12499999999999993 -0.2499999999999999 -0.12499999999999994 0.4999999999999998 0.12499999999999994;
            -0.12499999999999994 -0.24999999999999983 -0.12499999999999994 -0.2499999999999999 0.12499999999999994 -4.163336342344337e-17 0.12499999999999994 0.4999999999999998] )
    println( "AssembleElementK Test 1 FAILED" )
    println(K_e)
    all_success = false
else
    println( "AssembleElementK Test 1 SUCCESS" )
end

# Unit testing AssembleGlobalK (test 1)
processBoundaryConditions(grid) 
AssembleGlobalKAndFint(grid)
if(grid.K != [0.4999999999999998 -0.2499999999999999; -0.2499999999999999 0.4999999999999998])
    println( "AssembleGlobalK Test 1 FAILED" )
    println("Global K:")
    println(grid.K)
    all_success = false
else
    println( "AssembleGlobalK Test 1 SUCCESS" )
end

#************
### GRID 6
#************

displacementConstraint =    function(grid::Grid2D,e,a,dofi)
                                if(dofi == 1 && grid.nodes[dofi,grid.IEN[a,e]] == grid.M)  
                                    # Far right edge has displacement BC of 1.0
                                    return true,1.0
                                elseif(grid.nodes[1,grid.IEN[a,e]] == 0.0)
                                    # Far left edge has displacement BC of 0.0 in both x and y directions.
                                    return true,0.0
                                end
                                return false,0.0
                            end 

# m: Number of elements (x-direction)
# Mmin: Start x-value of the geometry
# M: Width of x-direction
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# E: Young's Modulus
# nu: Poisson's Ratio
grid = Grid2D(1,0.,1.,2,
            1,0.,1.,2,
            1.,0.,
            displacementConstraint,tractionForElement)

# Unit testing AssembleGlobalK (test 2)
processBoundaryConditions(grid)                            
AssembleGlobalKAndFint(grid)
if(grid.K != [1.3333333333333337 3.5887091909270197e-17 -0.16666666666666674 -0.35555555555555585 -1.0842021724855044e-17 -0.22222222222222218 -0.08888888888888885 1.734723475976807e-18 0.055555555555555566;
              3.5887091909270197e-17 1.6 -0.022222222222222143 4.683753385137379e-17 -1.244444444444445 -0.26666666666666683 -4.336808689942018e-18 0.08888888888888902 0.06666666666666671;
              -0.16666666666666674 -0.022222222222222143 0.46666666666666684 0.22222222222222227 -0.2666666666666668 -0.27777777777777807 -0.05555555555555558 0.06666666666666674 0.005555555555555588;
              -0.35555555555555585 4.683753385137379e-17 0.22222222222222227 4.266666666666666 -1.214306433183765e-17 -2.2551405187698492e-17 -0.3555555555555554 -1.3877787807814457e-17 -0.2222222222222222;
              -1.0842021724855044e-17 -1.244444444444445 -0.2666666666666668 -1.214306433183765e-17 4.266666666666667 -0.3555555555555552 3.469446951953614e-17 -1.2444444444444442 -0.26666666666666666;
              -0.22222222222222218 -0.26666666666666683 -0.27777777777777807 -2.2551405187698492e-17 -0.3555555555555552 1.3333333333333337 0.22222222222222218 -0.26666666666666705 -0.2777777777777779;
              -0.08888888888888885 -4.336808689942018e-18 -0.05555555555555558 -0.3555555555555554 3.469446951953614e-17 0.22222222222222218 1.3333333333333337 -4.163336342344337e-17 0.1666666666666667;
              1.734723475976807e-18 0.08888888888888902 0.06666666666666674 -1.3877787807814457e-17 -1.2444444444444442 -0.26666666666666705 -4.163336342344337e-17 1.6 -0.022222222222222164;
              0.055555555555555566 0.06666666666666671 0.005555555555555588 -0.2222222222222222 -0.26666666666666666 -0.2777777777777779 0.1666666666666667 -0.022222222222222164 0.46666666666666656])
    println( "AssembleGlobalK Test 2 FAILED" )
    println("Global K:")
    println(grid.K)
    all_success = false
else
    println( "AssembleGlobalK Test 2 SUCCESS" )
end



#*******
#***TEST RESULT***
#*******

println()
if(all_success)
    println("All Tests SUCCESS")
else
    println("Some tests FAILED")
end
println()


#**
#*****
#***********
#****************
#*************************
#**** PLOTS **************
#*************************
#****************
#***********
#*****
#**

#show_plots = true
show_plots = false

if( show_plots )

    #**********************************************
    #**** BasisParamSpace (1D) Test, p=1 **********
    #**********************************************

    print("BasisParamSpace (1D) Test...\n")
    n = 100
    p = 1
    xi = [linspace(-1,1,n)[i] for i=1:n]
    PyPlot.figure()
    for plot_num=1:(p+1)
        f = [BasisParamSpace(p,xi[i])[plot_num] for i=1:n]
        print("Plotting plot number ",plot_num,"\n")
        PyPlot.plot(xi,f)
    end
    PyPlot.title("BasisParamSpace (1D) Test, p=1")

    #**********************************************
    #**** DelBasisParamSpace (1D) Test, p=1 *******
    #**********************************************

    print("DelBasisParamSpace (1D) Test...\n")
    n = 100
    p = 1
    xi = [linspace(-1,1,n)[i] for i=1:n]
    PyPlot.figure()
    for plot_num=1:(p+1)
        f = [DelBasisParamSpace(p,xi[i])[plot_num] for i=1:n]
        print("Plotting plot number ",plot_num,"\n")
        PyPlot.plot(xi,f)
    end
    PyPlot.title("DelBasisParamSpace (1D) Test, p=1")

    #**********************************************
    #**** BasisParamSpace (1D) Test, p=2 **********
    #**********************************************

    n = 100
    p = 2
    xi = [linspace(-1,1,n)[i] for i=1:n]
    PyPlot.figure()
    for plot_num=1:(p+1)
        f = [BasisParamSpace(p,xi[i])[plot_num] for i=1:n]
        print("Plotting plot number ",plot_num,"\n")
        PyPlot.plot(xi,f)
    end
    PyPlot.title("BasisParamSpace (1D) Test, p=2")

    #**********************************************
    #**** DelBasisParamSpace (1D) Test, p=2 *******
    #**********************************************

    n = 100
    p = 2
    xi = [linspace(-1,1,n)[i] for i=1:n]
    PyPlot.figure()
    f = 0
    for plot_num=1:(p+1)
        f = [DelBasisParamSpace(p,xi[i])[plot_num] for i=1:n]
        print("Plotting plot number ",plot_num,"\n")
        PyPlot.plot(xi,f)
    end
    PyPlot.title("DelBasisParamSpace (1D) Test, p=2")


    #*****************************************
    #**** BasisParamSpace (2D) Test **********
    #*****************************************
    print("BasisParamSpace (2D) Test...\n")
    n = 25
    p = 1
    q = 1
    xi = [linspace(-1,1,n)[i] for i=1:n]
    eta = [linspace(-1,1,n)[i] for i=1:n]
    X = [xi[j] for i=1:n, j=1:n]
    Y = [eta[i] for i=1:n, j=1:n]

    fig = PyPlot.figure()
    ax = gca(projection="3d")
    Z = [BasisParamSpace(p,q,xi[i],eta[j])[4] for i=1:n, j=1:n]
    plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=1, antialiased=false)
    PyPlot.title("BasisParamSpace (2D) Test, [4]")

    fig = PyPlot.figure()
    ax = gca(projection="3d")
    Z = [BasisParamSpace(p,q,xi[i],eta[j])[2] for i=1:n, j=1:n]
    plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=1, antialiased=false)
    PyPlot.title("BasisParamSpace (2D) Test, [2]")


    #*****************************************
    #**** DelBasisParamSpace (2D) Test **********
    #*****************************************
    print("DelBasisParamSpace (2D) Test...\n")
    n = 25
    p = 1
    q = 1
    xi = [linspace(-1,1,n)[i] for i=1:n]
    eta = [linspace(-1,1,n)[i] for i=1:n]
    X = [xi[j] for i=1:n, j=1:n]
    Y = [eta[i] for i=1:n, j=1:n]

    Z_xi = zeros(n,n)
    Z_eta = zeros(n,n)
    for i=1:n
        for j=1:n
           DEL_XI_ETA = DelBasisParamSpace(p,q,xi[i],eta[j])
           Z_xi[i,j] = DEL_XI_ETA[1,4]
           Z_eta[i,j] = DEL_XI_ETA[2,4]
        end
    end

    fig = PyPlot.figure()
    ax = gca(projection="3d")
    plot_surface(X, Y, Z_xi, rstride=1, cstride=1, linewidth=1, antialiased=false)
    PyPlot.title("DelBasisParamSpace (2D) Test, Xi, [4]")

    fig = PyPlot.figure()
    ax = gca(projection="3d")
    plot_surface(X, Y, Z_eta, rstride=1, cstride=1, linewidth=1, antialiased=false)
    PyPlot.title("DelBasisParamSpace (2D) Test, Eta, [4]")

end







