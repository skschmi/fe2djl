# Steven Schmidt
# FE 607
# Coding Assignment 1
# March, 2016

include("../fe_2d.jl")

function body_heat_f(x)
    if(bodyheat_i == 1)
        return 1.0
    elseif(bodyheat_i == 2)
        return x
    elseif(bodyheat_i == 3)
        return x^2
    else
        return 0.0
    end
end


# Computing the folowing expression:
#  \int_{\Omega_{e}}N_{i}N_{a}f_{a}d\Omega_{e}
FextForElement =
    function(self::Grid1D,e)
        Fext = zeros(self.nnodesperel)
        jacobianf = function(params)
                        a = self.nodes[self.IEN[1,e]]
                        b = self.nodes[self.IEN[self.nnodesperel,e]]
                        return (b-a)/2
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = BasisParamSpace(self.p,xi)
                    fofx = body_heat_f(ParamToReal(self,e,xi))
                    ans = N[node_a]*fofx
                    return ans
                end
            Fext[node_a] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
        end
        return reshape(Fext,self.ndofpernode,self.nnodesperel)
    end

# Computing the folowing expression:
#  \int_{\Omega_{e}}B_{a}B_{i}d_{i}\left(N_{j}d_{j}N_{k}d_{k}+1\right)d\Omega_{e}
NForElement =
    function(self::Grid1D,e)
        Fint = zeros(self.nnodesperel)
        jacobianf = function(params)
                        a = self.nodes[self.IEN[1,e]]
                        b = self.nodes[self.IEN[self.nnodesperel,e]]
                        return (b-a)/2
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = BasisParamSpace(self.p,xi)
                    B = DelBasisRealSpace(self,e,xi)
                    sumNjdj = 0.0
                    for node_j = 1:self.nnodesperel
                        sumNjdj += N[node_j]*self.d[1,self.IEN[node_j,e]]
                    end
                    sumNkdk = sumNjdj
                    sumBidi = 0.0
                    for node_i = 1:self.nnodesperel
                        sumBidi += B[node_i]*self.d[1,self.IEN[node_i,e]]
                    end

                    ans = B[node_a] * sumBidi * (sumNjdj * sumNkdk + 1)

                    #***********************
                    # If we set Kappa=1:
                    #ans = B[node_a] * sumBidi * (1)
                    #***********************

                    return ans
                end
            Fint[node_a] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
        end
        return reshape(Fint,self.ndofpernode,self.nnodesperel)
    end

function KpqForElement(self::Grid1D,e::Int64,node_p::Int64,node_q::Int64)
    jacobianf = function(params)
                    a = self.nodes[self.IEN[1,e]]
                    b = self.nodes[self.IEN[self.nnodesperel,e]]
                    answer = (b-a)/2
                    return answer
                end
    f = function(params)
            xi = params[1]

            N = BasisParamSpace(self.p,xi)
            B = DelBasisRealSpace(self,e,xi)

            sumNjdj = 0.0
            for node_j = 1:self.nnodesperel
                sumNjdj += N[node_j]*self.d[1,self.IEN[node_j,e]]
            end
            sumNkdk = sumNjdj

            sumBidi = 0.0
            for node_i = 1:self.nnodesperel
                sumBidi += B[node_i]*self.d[1,self.IEN[node_i,e]]
            end

            ans = B[node_p]*N[node_q]*sumBidi*(sumNjdj+sumNkdk) + B[node_p]*B[node_q]*(sumNjdj*sumNkdk+1)

            #***********************
            # If we set Kappa=1:
            #ans = B[node_p]*B[node_q]
            #***********************

            return ans
        end
    Kpq = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
end

dirichletConstraint =
            function(self::Grid1D,e,a,dofi)
                #Setting bc that u(1)=0
                if(e==self.nelements && a==self.p+1)
                    return true,0.0
                end
                return false,0.0
            end

neumannBCForElement =
            function(self::Grid1D,e)
                T = zeros(self.p+1)
                return T
            end

consistentTangentForElement =
            function(self::Grid1D,e)
                Kel = Array(Any,self.nnodesperel,self.nnodesperel)
                for nd_p = 1:self.nnodesperel
                    for nd_q = 1:self.nnodesperel
                        entry = Array{Float64}([KpqForElement(self::Grid1D,e,nd_p,nd_q)])
                        Kel[nd_p,nd_q] = entry
                    end
                end
                return Kel
            end



# Always constants
Mmin = 0.0
M = 1.0
E = 1.0     # E: Young's Modulus
nu = 0.0    # nu: Poisson's Ratio
ndofpernode = 1
bodyheat_titles = ["f=1.0","f=x","f=x^2"]

#Iterate for various runs.
m = 100      # Do for 1,10,100,1000 elements
p = 1       # Do for p = 1,2
bodyheat_i = 3


grid = Grid1D(m,Mmin,M,p,ndofpernode,
            E,nu,
            dirichletConstraint,
            neumannBCForElement,
            FextForElement,
            NForElement,
            consistentTangentForElement)

println("Running: Solve(grid)")
Solve(grid)

import GR
h = (1/100)
if(m>=10)
    h = (1/(m*10))
end
x = (Mmin:h:Mmin+M)[1:end-1]
function getTemperature(self::Grid1D,x)
    e,xi = RealToElementParam(self,x)
    return GetValue(grid,"temperature",e,xi)
end
temp = [getTemperature(grid,x[i])[1] for i = 1:length(x)]
GR.plot(x,temp)
GR.ylim((minimum(temp)-0.1,maximum(temp)+0.1))
GR.title("Num elements="*string(grid.m)*", p="*string(grid.p)*", Body heat "*bodyheat_titles[bodyheat_i])
GR.xlabel("x")
GR.ylabel("Temperature")



import GR

if(true)

    # Always constants
    Mmin = 0.0
    M = 1.0
    E = 1.0     # E: Young's Modulus
    nu = 0.0    # nu: Poisson's Ratio
    ndofpernode = 1
    bodyheat_titles = ["f=1.0","f=x","f=x^2"]
    #colors=["g","b","r","y"]

    #Iterate for various runs.
    for bodyheat_i = 1:3
        for p in [1,2]
            maxtemp = 0.0
            mintemp = 1000000
            GR.figure()
            for m in [1,10,100,1000]
                grid = Grid1D(m,Mmin,M,p,ndofpernode,
                            E,nu,
                            dirichletConstraint,
                            neumannBCForElement,
                            FextForElement,
                            NForElement,
                            consistentTangentForElement)

                println("Running: Solve(grid)")
                Solve(grid)

                h = (1/100)
                if(m>=10)
                    h = (1/(m*10))
                end
                x = (Mmin:h:Mmin+M)[1:end-1]
                function getTemperature(self::Grid1D,x)
                    e,xi = RealToElementParam(self,x)
                    return GetValue(grid,"temperature",e,xi)
                end
                temp = [getTemperature(grid,x[i])[1] for i = 1:length(x)]
                if(maximum(temp) > maxtemp)
                    maxtemp = maximum(temp)
                end
                if(minimum(temp) < mintemp)
                    mintemp = minimum(temp)
                end
                GR.plot(x,temp)
                GR.hold(true)
            end
            GR.hold(false)
            GR.ylim((mintemp-0.02,maxtemp+0.02))
            GR.title("p="*string(grid.p)*", body heat "*bodyheat_titles[bodyheat_i])
            GR.legend("1 elem","10 elem","100 elem","1000 elem")
            GR.xlabel("x")
            GR.ylabel("Temperature")
            GR.savefig("plots/plot_p"*string(p)*"_bh"*string(bodyheat_i)*".pdf")
        end
    end

end
