#Steven Schmidt
#FE507 HW5 Problem 2

using PyPlot
include("../fe_2d.jl")


# Step 1: Set up the boundary condition functions

P = 1000. # pressure

# Specify the displacement constraint
displacementConstraint =    
            function(grid::Grid2D,e,a,dofi)
                # The bottom edge (theta = 0) can shift in the r-direction but is fixed in theta
                # The top edge (theta = pi/2) can also shift in the r-direction, but is fixed in theta
                # (That means the bottom is fixed in y, free in x;
                #  and the top is fixed in x, free in y)
                x = grid.nodes[1,grid.IEN[a,e]]
                y = grid.nodes[2,grid.IEN[a,e]]
                r,theta = Polar(x,y)
                if( dofi == 1 && theta >= (grid.Nmin+grid.N) - (1e-10) )  
                    # Far left edge has displacement BC of 0.0 in the x direction
                    return true,0.0
                elseif( dofi == 2 && theta <= grid.Nmin + (1e-10) )
                    # Bottom edge has displacement BC of 0.0 in the y direction
                    return true,0.0
                end
                return false,0.0
            end

# Specify the traction on each element
tractionForElement =    
            function(self::Grid2D,e)
                T = zeros(2,(self.p+1)*(self.q+1))
                # If the element is on the far left side of the domain, then it has a traction.
                if( e % m == 1 )
                    nde_i = 1
                    for nde_j = 1:(self.q+1)
                        node_index_e = IndexForRowCol(nde_i,nde_j,(self.p+1))
                        node_index_global = self.IEN[node_index_e,e]
                        node_coords = self.nodes[:,node_index_global]
                        normal_vec = (node_coords / norm(node_coords))
                        T[1,node_index_e] = P*normal_vec[1]
                        T[2,node_index_e] = P*normal_vec[2]
                    end
                end
                return T
            end



# Step 2: Initialize the grid
m = 10      # m: Number of elements (r-direction)
Mmin = 1.0  # Mmin: minimum r-value of the geometry
M = 1.0     # M: Width of r-direction
p = 2       # p: Polynomial degree (r-drection)
n = 10      # n: Number of elements (theta-direction)
Nmin = 0.0  # Nmin starting point of the theta-value of the geometry
N = pi/2    # N: Height of theta-direction (in radians)
q = 2       # q: Polynomial degree (theta-direction)
E = 1.e7    # E: Young's Modulus
nu = 0.3    # nu: Poisson's Ratio
polarcoords = true #Using polar coordinates
grid = Grid2D(m,Mmin,M,p,
            n,Nmin,N,q,
            E,nu,
            displacementConstraint,tractionForElement,
            polarcoords)




# Step 3: Solve the FE problem
Solve(grid)




# Step 4: Plotting

# Plotting
nx = 5
ny = 5

folderpathprefix = "plots/"
prefix = "hw5p2"
plot_the_plots = true

# Plotting the geometry
if( plot_the_plots )
    value_string = "geometry"
    fig = PyPlot.figure()
    ax = gca()
    p = plot(grid.nodes[1,:],grid.nodes[2,:],"*")
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end

# Plot the exact solution
value_string = "d_r_exact"
X,Y,Zzero = Plot(grid,"",nx,ny) # Getting the x,y values to evaluate
R,THETA = Polar(X,Y)
Ri = grid.Mmin
Ro = grid.Mmin+grid.M
U_R = (1./E).*((P.*Ri.^2)./(Ro.^2-Ri.^2)).*((1-nu).*R + ((Ro.^2.*(1+nu))./R))
if( plot_the_plots )
    fig = PyPlot.figure()
    ax = gca()
    p = pcolor(X, Y, U_R) #vmin=-0.1, vmax=1.1
    colorbar(p, ax=ax)
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end

# Plotting the FE results
value_string = "d_x"
X,Y,Zdx = Plot(grid,value_string,nx,ny)
if( plot_the_plots )
    fig = PyPlot.figure()
    ax = gca()
    p = pcolor(X, Y, Zdx) #vmin=-0.1, vmax=1.1
    colorbar(p, ax=ax)
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end

value_string = "d_y"
X,Y,Zdy = Plot(grid,value_string,nx,ny)
if( plot_the_plots )
    fig = PyPlot.figure()
    ax = gca()
    p = pcolor(X, Y, Zdy) #vmin=-0.1, vmax=1.1
    colorbar(p, ax=ax)
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end

Zdr = sqrt(Zdx.^2 + Zdy.^2)
value_string = "d_r"
if( plot_the_plots )
    fig = PyPlot.figure()
    ax = gca()
    p = pcolor(X, Y, Zdr) #vmin=-0.1, vmax=1.1
    colorbar(p, ax=ax)
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end


Error = abs(Zdr-U_R)
err_max = maximum(Error)
println("err_max: ",err_max)
value_string = "Error"
if( plot_the_plots )
    fig = PyPlot.figure()
    ax = gca()
    p = pcolor(X, Y, Error) #vmin=-0.1, vmax=1.1
    colorbar(p, ax=ax)
    PyPlot.title(prefix*" "*value_string)
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")
end

