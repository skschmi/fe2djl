#Steven Schmidt
#FE507 HW5 Problem 1b

using PyPlot
include("../fe_2d.jl")

# Specify the displacement constraint
displacementConstraint =    
            function(grid::Grid2D,e,a,dofi)
                #if(dofi == 1 && grid.nodes[dofi,grid.IEN[a,e]] == grid.M)  
                #    # Far right edge has the displacement BC specified here (x-direction only)
                #    return true,1.0
                #end
                if(grid.nodes[1,grid.IEN[a,e]] == 0.0)
                    # Far left edge has displacement BC of 0.0 in both x and y directions.
                    return true,0.0
                end
                return false,0.0
            end

# Specify the traction on each element
tractionForElement =    
            function(self::Grid2D,e)
                T = zeros(2,(self.p+1)*(self.q+1))
                # If the element is on the far right side of the domain, then it has a traction.
                if( e % m == 0 )
                    nde_i = self.p+1
                    dim = 1
                    for nde_j = 1:(self.q+1)
                        T[dim,IndexForRowCol(nde_i,nde_j,(self.p+1))] = 1000.0
                    end
                end
                return T
            end

# Initialize the grid
m = 10       # m: Number of elements (x-direction)
Mmin = 0.0  # Mmin: Start x-value of the geometry
M = 1.0     # M: Width of x-direction
p = 2       # p: Polynomial degree (x-drection)
n = 10       # n: Number of elements (y-direction)
Nmin = 0.0  # Nmin: Start y-value of the geometry
N = 1.0     # N: Height of y-direction
q = 2       # q: Polynomial degree (y-direction)
E = 1.0e7     # E: Young's Modulus
nu = 0.3    # nu: Poisson's Ratio
grid = Grid2D(m,Mmin,M,p,n,Nmin,N,q,E,nu,displacementConstraint,tractionForElement)

# Solve the FE problem
Solve(grid)

nx = 5
ny = 5

folderpathprefix = "plots/"
prefix = "hw5p1b"

value_string = "geometry"
fig = PyPlot.figure()
ax = gca()
p = plot(grid.nodes[1,:],grid.nodes[2,:],"*")
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")

value_string = "d_x"
X,Y,Zdx = Plot(grid,value_string,nx,ny)
fig = PyPlot.figure()
ax = gca()
p = pcolor(X, Y, Zdx) #vmin=-0.1, vmax=1.1
colorbar(p, ax=ax)
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")

value_string = "d_y"
X,Y,Zdy = Plot(grid,value_string,nx,ny)
fig = PyPlot.figure()
ax = gca()
p = pcolor(X, Y, Zdy) #vmin=-0.1, vmax=1.1
colorbar(p, ax=ax)
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")

value_string = "s_xx"
X,Y,Zsxx = Plot(grid,value_string,nx,ny)
fig = PyPlot.figure()
ax = gca()
p = pcolor(X, Y, Zsxx) #vmin=-0.1, vmax=1.1
colorbar(p, ax=ax)
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")

value_string = "s_yy"
X,Y,Zsyy = Plot(grid,value_string,nx,ny)
fig = PyPlot.figure()
ax = gca()
p = pcolor(X, Y, Zsyy) #vmin=-0.1, vmax=1.1
colorbar(p, ax=ax)
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")

value_string = "s_xy"
X,Y,Zsxy = Plot(grid,value_string,nx,ny)
fig = PyPlot.figure()
ax = gca()
p = pcolor(X, Y, Zsxy) #vmin=-0.1, vmax=1.1
colorbar(p, ax=ax)
PyPlot.title(prefix*" "*value_string)
PyPlot.xlabel("x")
PyPlot.ylabel("y")
PyPlot.savefig(folderpathprefix*prefix*"_"*value_string*".pdf")





