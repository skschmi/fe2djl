
# Util for Gaussian Quadrature
include("./ssgaussquadjl/gaussquad.jl")
# Library for indexing
include("./ssindexingtools/indexingtools.jl")
using SSGaussQuad
import SSGaussQuad.GaussQuadTable
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts
using SSIndexingTools
import SSIndexingTools.IndexForRowCol
import SSIndexingTools.RowColForIndex

# ----- KroneckerDelta -----
# Returns: The value of the Kronecker Delta function given i,j
function KroneckerDelta(i,j)
    return (i==j)
end

# ----- Polar -----
# Returns: Polar coordinates (r,theta) for a given (x,y)
function Polar(x,y)
    return sqrt(x.^2 + y.^2), atan(y./x)
end

# ----- ElasticCoeffsCijkl -----
# Returns: The coefficient in Cijkl at the specified index i,j,k,l
function ElasticCoeffsCijkl(E,nu,i,j,k,l)
    lambda = nu*E/((1+nu)*(1-2*nu))
    mu = E/(2*(1+nu))
    return mu * (KroneckerDelta(i,k)*KroneckerDelta(j,l) + KroneckerDelta(i,l)*KroneckerDelta(j,k)) +
                    lambda * KroneckerDelta(i,j) * KroneckerDelta(k,l)
end

# ----- IJ_to_ijkl -----
# Returns: The index conversion from D[I,J] to C[i,j,k,l]
function IJ_to_ijkl(I,J)
    chart = [1 1; 2 2; 2 1];
    return chart[I,1],chart[I,2],chart[J,1],chart[J,2]
end

# ----- NodesPerDimension(m,p) -----
# m: Number of elements (dimension-direction)
# p: Polynomial degree (dimension-direction)
# Returns: The number of nodes wide (or long), given the number
#          of elements and the polynomial degree of each element.
function NodesPerDimension(m,p)
    return (m+1)+m*(p-1)
end

# ----- BuildD -----
# E: Young's Modulus
# nu: Poisson's Ratio
# Returns: The 3x3 D matrix, equivalent (with some assumptions) to the Cijkl elastic coeffs tensor.
function BuildD(E,nu)
    D = zeros(3,3)
    for J = 1:3
        for I = 1:3
            i,j,k,l = IJ_to_ijkl(I,J)
            D[I,J] = ElasticCoeffsCijkl(E,nu,i,j,k,l)
        end
    end
    return D
end

# ----- LagrangeNodes (1D) -----
# m: Number of elements (x-direction)
# Mmin: Start x-value of the geometry
# M: Width of x-direction
# p: Polynomial degree (x-drection)
# Returns: Dimension 1-by-nnodes array, each entry containing the x coordinate of the node.
function LagrangeNodes(m,Mmin,M,p)
    imax = (m+1)+m*(p-1)
    X = collect(linspace(Mmin,Mmin+M,imax))
    return X
end

# ----- LagrangeNodes (2D) -----
# m: Number of elements (x-direction)
# Mmin: Start x-value of the geometry
# M: Width of x-direction
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# Nmin: Start y-value of the geometry
# N: Height of y-direction
# q: Polynomial degree (y-direction)
# polarcoords: Optional argument, default is false; if true, then
#    the computed node locations are assumed to be in (r,theta) coordinates,
#    and are converted to (x,y) under that assumption before being returned.
# Returns: Dimension 2-by-nnodes matrix, each column containing the x,y coordinates of the node.
function LagrangeNodes(m,Mmin,M,p,n,Nmin,N,q,polarcoords = false)
    imax = (m+1)+m*(p-1)
    jmax = (n+1)+n*(q-1)
    X = [ linspace(Mmin,Mmin+M,imax)[i] for i=1:imax, j=1:jmax ]
    Y = [ linspace(Nmin,Nmin+N,jmax)[j] for i=1:imax, j=1:jmax ]
    NODES = [reshape(X,(1,length(X)));reshape(Y,(1,length(Y)));]
    if(polarcoords)
        temp = zeros(size(NODES))
        temp[1,:] = NODES[1,:].*cos(NODES[2,:])
        temp[2,:] = NODES[1,:].*sin(NODES[2,:])
        NODES = temp
    end
    return NODES
end

# ----- LagrangeIEN (1D) -----
# e: The element index (first index is 1)
# p: Polynomial degree (x-direction)
# Returns: Array containing the indices of each node in the specified element
function LagrangeIEN(e,p)

    # Number of nodes per element
    nodes_per_element = NodesPerDimension(1,p)

    # Initialize the array
    IEN = zeros( Int64, nodes_per_element )

    node_i = (e-1)*(nodes_per_element-1)+1
    for i = 1:nodes_per_element
        IEN[i] = node_i
        node_i += 1
    end

    return IEN
end

# ----- LagrangeIEN (2D) -----
# e: The element index (first index is 1)
# m: Number of elements (x-direction)
# p: Polynomial degree (x-direction)
# q: Polynomial degree (y-direction)
# Returns: Array containing the indices of each node in the specified element
function LagrangeIEN(e,m,p,q)

    # Number of nodes per row
    nodes_in_x = (m+1)+m*(p-1)

    # element x_index, y_index
    e_x_index, e_y_index = RowColForIndex(e,m)

    # node row, col minimum and maximum
    node_xmin = ((e_x_index-1) + (e_x_index-1)*(p-1)) + 1
    node_xmax = (e_x_index + e_x_index*(p-1)) + 1
    node_ymin = ((e_y_index-1) + (e_y_index-1)*(q-1)) + 1
    node_ymax = (e_y_index + e_y_index*(q-1)) + 1

    #println("node_xmin: ",node_xmin, " node_xmax: ", node_xmax, " node_ymin: ",node_ymin," node_ymax: ",node_ymax)

    #Result is an array the length of the number of nodes in the element
    elnum = 1
    IEN = zeros( Int64, (node_xmax-node_xmin+1)*(node_ymax-node_ymin+1) )
    for j=node_ymin:node_ymax
        for i=node_xmin:node_xmax
            node_index = IndexForRowCol(i,j,nodes_in_x)
            IEN[elnum] = convert(Int64,floor(node_index))
            elnum += 1
        end
    end

    return IEN
end


# ----- BasisParamSpace (1D) -----
# p: Polynomial Order
# xi: Coordinate to evaluate
# Returns: Array of p+1 values, evaluated at xi
#          for each of the p+1 basis functions
#          in parameter space.
function BasisParamSpace(p,xi)
    nodes = linspace(-1,1,(p+1))
    A = zeros(length(nodes))
    for i = 1:(p+1)
        prod = 1
        for j = 1:(p+1)
            if(j != i)
                prod *= (xi - nodes[j])/(nodes[i]-nodes[j])
            end
        end
        A[i] = prod
    end
    return A
end

# ----- value_of -----
# Evaluating subset of terms in basis function equation i at xi.
function value_of(xi, nodes, i, start_j, end_j)
    prod = 1
    for j = start_j:end_j
        if(i != j)
            prod *= ((xi - nodes[j])/(nodes[i]-nodes[j]))
        end
    end
    return prod
end

# ----- derivative_of -----
# Evaluating the derivative of a subset of terms in basis function i at xi.
# xi: Value at which the derivative is evaluated
# nodes: Array of the xi locations of the nodes between -1 and 1
# i: The i-th basis function
# start_j: The start index of the subset of terms in the basis function
# end_j: The end index of the subset of terms in the basis function
function derivative_of(xi, nodes, i, start_j, end_j)
    if(start_j == end_j)
        j = start_j
        if( i != j )
            return 1/(nodes[i]-nodes[j])
        else
            return 0
        end
    end

    # Product rule: (first * deriv of second) + (second * deriv of first)
    return value_of(xi, nodes, i, start_j, end_j-1)*derivative_of(xi, nodes, i, end_j, end_j) +
           value_of(xi, nodes, i, end_j, end_j)*derivative_of(xi, nodes, i, start_j, end_j-1)
end


# ----- DelBasisParamSpace (1D) -----
# p: Polynomial Order
# xi: Coordinate to evaluate
# Returns: Array of p+1 values, evaluated at xi
#          for each of the p+1 derivative of the
#          basis functions in parameter space.
function DelBasisParamSpace(p,xi)
    nodes = linspace(-1,1,(p+1))
    A = zeros(length(nodes))
    for i = 1:(p+1)
        A[i] = derivative_of(xi, nodes, i, 1, length(nodes))
    end
    return A
end


# ----- BasisParamSpace (2D) -----
# p: polynomial order (xi-direction)
# q: polynomial order (eta-direction)
# xi: Coordinate to evaluate (xi-direction)
# eta: Coordinate to evaluate (eta-direction)
# Returns: Matrix of dimension 1-by-((p+1)*(q+1)) containing values evaluated
#          at (xi,eta) for each basis function.
function BasisParamSpace(p,q,xi,eta)
    xi_array = BasisParamSpace(p,xi)
    eta_array = BasisParamSpace(q,eta)
    A = [ xi_array[i]*eta_array[j] for i=1:(p+1), j=1:(q+1) ]
    return reshape(A,length(A))
end


# ----- DelBasisParamSpace (2D) -----
# p: polynomial order (xi-direction)
# q: polynomial order (eta-direction)
# xi: Coordinate to evaluate (xi-direction)
# eta: Coordinate to evaluate (eta-direction)
# Returns: Matrix, dimension 2-by-((p+1)*(q+1)); The first row contains dN/dxi for each basis
#          function.  The second row contains dN/deta for each basis function.
function DelBasisParamSpace(p,q,xi,eta)

    DNDXI_DNDETA = zeros(2,(p+1)*(q+1))

    xi_array = DelBasisParamSpace(p,xi)
    eta_array = BasisParamSpace(q,eta)

    for j=1:(q+1)
        for i=1:(p+1)
            DNDXI_DNDETA[1,IndexForRowCol(i,j,(p+1))] = xi_array[i]*eta_array[j]
        end
    end

    xi_array = BasisParamSpace(p,xi)
    eta_array = DelBasisParamSpace(q,eta)

    for j=1:(q+1)
        for i=1:(p+1)
            DNDXI_DNDETA[2,IndexForRowCol(i,j,(p+1))] = xi_array[i]*eta_array[j]
        end
    end

    return DNDXI_DNDETA
end


# ----- FieldValue (1D) -----
# Returns the value of the field at xi,eta
# fvals: The values of the cooefs at each of the nodes in the element
# p: polynomial order (xi-direction)
# xi: Coordinate to evaluate (xi-direction)
function FieldValue(fvals,p,xi)
    return sum(BasisParamSpace(p,xi)[:] .* fvals[:])
end


# ----- FieldValue (2D) -----
# Returns the value of the field at xi,eta
# fvals: The values of the cooefs at each of the nodes in the element
# p: polynomial order (xi-direction)
# q: polynomial order (eta-direction)
# xi: Coordinate to evaluate (xi-direction)
# eta: Coordinate to evaluate (eta-direction)
function FieldValue(fvals,p,q,xi,eta)
    return sum(BasisParamSpace(p,q,xi,eta)[:] .* fvals[:])
end




# ----- Grid1D Type Definition -----
#
# *** Info about the grid: ***
# m: Number of elements (x-direction)
# M: Width of x-direction
# p: Polynomial degree (x-drection)
#
# *** Important constants: ***
# E: Young's Modulus
# nu: Poisson's Ratio
#
# *** Info for each node: ***
# nodes: Dimension 1-by-nnodes array, each entry containing the x coordinate of the node.
# nnodes: The number of nodes
# ID: Dimension ndofpernode-by-nnodes matrix, each column containing the index for each degree-of-freedom for each node.
#               If the value is 0, then that dof is constrained.  Otherwise, it is the index of the column/row into the K
#               matrix of that dof.
# d: Dimension ndofpernode-by-nnodes array, The first row contains the final x-direction displacement for each node
#
# *** Info for each element ***
# IEN: Dimension (n_nodes_per_element)-by-(nelements) matrix, containing the connectivity of each element.
#      The columns represent an element, with the nodes counted in column-major order y-min to y-max, then next x larger.
#
# *** Number of dof in the system (after BC are applied) ***
# dof: The total number of degrees of freedom
#
# *** For Gaussian Quadrature: ***
# GQT1Dp: Gaussian Quadrature table, 1D for order p.
#
# *** Callback functions for boundary conditions ***
# dirichletConstraint: Function with the signature (grid::Grid1D,e,a,dofi)
#     where:  grid: Pointer to the grid
#             e: The element index
#             a: The node index in the element.
#             dofi: The degree of freedom, 1=x, etc.
#             Returns: A tuple of type (Boolean,Float64).
#                      The boolean is true if the displacement is constrained for that dof, and is false otherwise.
#                      The Float64 value is the displacement for that dof, or ignored if the boolean is false.
#
# neumannBCForElement: Function with the signature (grid::Grid1D,e)
#    where:  grid: Pointer to the grid
#            e: The element index
#            Returns: Array of dimension 1-by(p+1),  contains the x-components of the tractions on the surface.
#                     (A value for each node in the element is returned; Only nodes on the surface should be non-zero)
#
# *** Callback functions for Newton-Raphson algorithm ***
# FextForElement: Function with the signature (grid::Grid1D,e)
#    where: grid: Pointer to the grid
#           e: The element index
#           Returns: Array of dimension ndofpernode-by-nnodesperel, The value is Fext
#
# NForElement: Function with the signature (grid::Grid1D,e)
#    where: grid: Pointer to the grid
#           e: The element index
#           Returns: Array of dimension ndofpernode-by-nnodesperel, The value is N, or Fint
#
# consistentTangentForElement: Function with the signature (grid::Grid1D,e)
#    where: grid: Pointer to the grid
#           e: The element index
#           Returns: The value of the consistent tangent (slope) of the load-displacement curve, for element e.
#               Return type: A nnodesperel-by-nnodesperel type Array{Any} matrix, containing at each index a
#               size ndofpernode-by-ndofpernode type Array{Float64} matrix.
type Grid1D
    # Info about the grid:
    m::Int64
    Mmin::Float64
    M::Float64
    p::Int64

    # Important constants:
    E::Float64
    nu::Float64
    epsilon::Float64
    nloadsteps::Int64
    load_step_i::Int64
    max_correction_loops::Int64

    # Info for each node:
    nodes
    nnodes::Int64
    ID
    d #This "d" vector contains all displacements, including BC displacements

    # Info for each element:
    IEN
    nelements::Int64
    nnodesperel::Int64
    ndofpernode::Int64

    # Number of dof in the system (after BC are applied)
    dof::Int64

    #Values we save here for efficiency
    K     # Size dof-by-dof matrix
    R     # Length dof array
    old_R # Length dof array
    d_vec # Length dof array: contains only those displacements that are dof.
    del_d # Length dof array: The change in displacements
    Fext  # Length dof array
    Nvec  # Length dof array

    # For Gaussian Quadrature:
    GQT1Dp::GaussQuadTable

    # *** Callback functions for boundary conditions ***
    dirichletConstraint::Function
    neumannBCForElement::Function

    # *** Callback functions for Newton-Raphson algorithm ***
    FextForElement::Function
    NForElement::Function
    consistentTangentForElement::Function

    # ----- Grid1D Constructor -----
    # m: Number of elements (x-direction)
    # Mmin: Start x-value of the geometry
    # M: Width of x-direction
    # p: Polynomial degree (x-drection)
    # E: Young's Modulus
    # nu: Poisson's Ratio
    function Grid1D(m::Int64,Mmin::Float64,M::Float64,p::Int64,ndofpernode::Int64,
                  E::Float64,nu::Float64,
                  dirichletConstraint::Function,neumannBCForElement::Function,
                  FextForElement::Function, NForElement::Function,
                  consistentTangentForElement::Function;
                  epsilon=1e-12, nloadsteps=5, max_correction_loops=50)

        self = new()

        # Info about the grid:
        self.m = m   # m: Number of elements (x-direction)
        self.Mmin = Mmin # Mmin: Start x-value of the geometry
        self.M = M   # M: Width of x-direction
        self.p = p   # p: Polynomial degree (x-drection)
        self.nelements = m
        self.nnodesperel = (p+1)
        self.ndofpernode = ndofpernode # This is assumed to be 1 for now; In the future support more?

        # Important constants:
        self.E = E   # E: Young's Modulus
        self.nu = nu # nu: Poisson's Ratio
        self.epsilon = epsilon # machine error
        self.max_correction_loops = max_correction_loops

        # Tracking load-step iterations
        self.nloadsteps = nloadsteps
        self.load_step_i = 0

        # Info for each node:
        self.nodes = LagrangeNodes(m,Mmin,M,p)
        self.nnodes = length(self.nodes)
        self.ID = nothing #Not defined yet (needs constraints info)
        self.d = zeros(self.ndofpernode,self.nnodes)

        # Info for each element:
        self.IEN = zeros(Int64,self.nnodesperel,self.nelements)
        for e = 1:self.nelements
            self.IEN[:,e] = LagrangeIEN(e,p)
        end

        # Number of dof in the system (after BC are applied)
        self.dof = 0             # to be set later
        self.d_vec = nothing     # to be set later
        self.del_d = nothing     # to be set later
        self.R = nothing         # to be set later
        self.K = nothing         # to be set later
        self.old_R = nothing     # to be set later
        self.Fext = nothing      # to be set later
        self.Nvec = nothing      # to be set later

        # For Gaussian Quadrature:
        # TODO: THIS should be sufficient for the highest-polynomial-order of
        #   the function being evaluated!  It could be N is order p, but we're
        #   evaluating N*N or N*B or something, so it could be higher order than p
        #   that is needed.
        self.GQT1Dp = GaussQuadTable(NumGaussPts(p)+1)

        # Callback functions for boundary conditions
        self.dirichletConstraint = dirichletConstraint
        self.neumannBCForElement = neumannBCForElement

        # Callback functions for Fext, N, consistent tangent
        self.FextForElement = FextForElement
        self.NForElement = NForElement
        self.consistentTangentForElement = consistentTangentForElement

        return self
    end
end




# ----- Grid2D Type Definition -----
#
# *** Info about the grid: ***
# m: Number of elements (x-direction)
# M: Width of x-direction
# p: Polynomial degree (x-drection)
# n: Number of elements (y-direction)
# N: Height of y-direction
# q: Polynomial degree (y-direction)
#
# *** Important constants: ***
# E: Young's Modulus
# nu: Poisson's Ratio
#
# *** Info for each node: ***
# nodes: Dimension 2-by-nnodes matrix, each column containing the x,y coordinates of the node.
# nnodes: The number of nodes
# ID: Dimension 2-by-nnodes matrix, each column containing the dof index for the x-direction and y-direction for each node.
#               If the value is 0, then that dof is constrained.  Otherwise, it is the index of the column/row into the K
#               matrix of that dof.
# d: Dimension 2-by-nnodes matrix, The first row contains the final x-direction displacement for each node
#                                  the second row contains the final y-direction displacement for each node
#
# *** Info for each element ***
# IEN: Dimension (n_nodes_per_element)-by-(nelements) matrix, containing the connectivity of each element.
#      The columns represent an element, with the nodes counted in column-major order y-min to y-max, then next x larger.
#
# *** Info for the K matrix: ***
# D: Contains the D matrix
# dof: The total number of degrees of freedom
# K: K matrix
# Fint: Internal force vector
# Fbf: Body force vector
# Fext: Traction force vector
# d_vec: The displacement vector used when solving the linear system
#
# *** For Gaussian Quadrature: ***
# GQT2D: Gaussian Quadrature table, 2D.
# GQT1Dp: Gaussian Quadrature table, 1D for order p.
# GQT1Dq: Gaussian Quadrature table, 1D for order q.
#
# *** Callback functions for boundary conditions ***
# displacementConstraint: Function which the signature (grid::Grid2D,e,a,dofi)
#     where:  grid: Pointer to the grid
#             e: The element index
#             a: The node index in the element.
#             dofi: The degree of freedom, 1=x, 2=y
#             Returns: A tuple of type (Boolean,Float64).
#                      The boolean is true if the displacement is constrained for that dof, and is false otherwise.
#                      The Float64 value is the displacement for that dof, or ignored if the boolean is false.
# tractionForElement: Function with the signature (grid::Grid2D,e)
#    where:  grid: Pointer to the grid
#            e: The element index
#            Returns: Matrix of dimension 2-by((p+1)*(q+1)), first row contains the x-component, and the
#                     second row contains the y-component of the tractions on the surface.
#                     (A value for each node in the element is returned; Only nodes on the surface should be non-zero)
type Grid2D
    # Info about the grid:
    m::Int64
    Mmin::Float64
    M::Float64
    p::Int64
    n::Int64
    Nmin::Float64
    N::Float64
    q::Int64


    # Important constants:
    E::Float64
    nu::Float64

    # Info for each node:
    nodes
    nnodes::Int64
    ID
    d

    # Info for each element:
    IEN
    nelements::Int64
    nnodesperel::Int64

    # Info for the K matrix:
    D
    dof::Int64
    K
    Fint
    Fbf
    Fext
    d_vec

    # For Gaussian Quadrature:
    GQT2D::GaussQuadTable
    GQT1Dp::GaussQuadTable
    GQT1Dq::GaussQuadTable

    # *** Callback functions for boundary conditions ***
    displacementConstraint::Function
    tractionForElement::Function

    # ----- Grid2D Constructor -----
    # m: Number of elements (x-direction)
    # Mmin: Start x-value of the geometry
    # M: Width of x-direction
    # p: Polynomial degree (x-drection)
    # n: Number of elements (y-direction)
    # Nmin: Start y-value of the geometry
    # N: Height of y-direction
    # q: Polynomial degree (y-direction)
    # E: Young's Modulus
    # nu: Poisson's Ratio
    # polarcoords: Optional argument, defaults to false; if
    #              true, then x is r, and y is theta.
    function Grid2D(m::Int64,Mmin::Float64,M::Float64,p::Int64,
                  n::Int64,Nmin::Float64,N::Float64,q::Int64,
                  E::Float64,nu::Float64,
                  displacementConstraint::Function,tractionForElement::Function,
                  polarcoords = false)

        self = new()

        # Info about the grid:
        self.m = m   # m: Number of elements (x-direction)
        self.Mmin = Mmin # Mmin: Start x-value of the geometry
        self.M = M   # M: Width of x-direction
        self.p = p   # p: Polynomial degree (x-drection)
        self.n = n   # n: Number of elements (y-direction)
        self.Nmin = Nmin # Nmin: Start y-value of the geometry
        self.N = N   # N: Height of y-direction
        self.q = q   # q: Polynomial degree (y-direction)
        self.nelements = m*n
        self.nnodesperel = (p+1)*(q+1)

        # Important constants:
        self.E = E   # E: Young's Modulus
        self.nu = nu # nu: Poisson's Ratio

        # Info for each node:
        self.nodes = LagrangeNodes(m,Mmin,M,p,n,Nmin,N,q,polarcoords)
        self.nnodes = size(self.nodes)[2]
        self.ID = 0 #Not defined yet (needs constraints info)
        self.d = zeros(2,self.nnodes)

        # Info for each element:
        self.IEN = zeros(Int64,self.nnodesperel,self.nelements)
        for e = 1:self.nelements
            self.IEN[:,e] = LagrangeIEN(e,m,p,q)
        end

        # Info for the K matrix:
        self.D = BuildD(E,nu)
        self.dof = 0 #Not defined yet (needs constraints info)
        self.K = 0 #Not defined yet
        self.Fint = 0 #Not defined yet
        self.Fbf = 0 #Not defined yet
        self.Fext = 0 #Not defined yet
        self.d_vec = 0 #Not defined yet

        # For Gaussian Quadrature:
        # TODO: THIS should be sufficient for the highest-polynomial-order of
        #   the function being evaluated!  It could be N is order p, but we're
        #   evaluating N*N or N*B or something, so it could be higher order than p
        #   that is needed.
        self.GQT2D = GaussQuadTable(NumGaussPts(p)+NumGaussPts(q)+2,NumGaussPts(p)+NumGaussPts(q)+2)
        self.GQT1Dp = GaussQuadTable(NumGaussPts(p)+1)
        self.GQT1Dq = GaussQuadTable(NumGaussPts(q)+1)

        # Callback functions for boundary conditions
        self.displacementConstraint = displacementConstraint
        self.tractionForElement = tractionForElement

        return self
    end
end

# ----- ElementDofLookup ("LM") (1D or 2D) -----
# e: The element index
# a: The node of the element
# dofi: The index of the degree of freedom (1=x, 2=y)
# Returns the index of the degree of freedom of a node in an element
function ElementDofLookup(self,e,a,dofi)
    return self.ID[dofi,self.IEN[a,e]]
end


# ----- ParamToReal (1D) -----
# self: Pointer to the grid
# e: Element index
# xi: The xi coordinate to be evaluated
# Returns: Real-space coordinate (x) at parameter-space
#          coordinate (xi) for element e.
function ParamToReal(self::Grid1D,e,xi)
    x = 0
    basisf = BasisParamSpace(self.p,xi)
    n_element_nodes = length(basisf)
    for a = 1:n_element_nodes
        x += basisf[a] * self.nodes[self.IEN[a,e]]
    end
    return x
end


# ----- ParamToReal (2D) -----
# self: Pointer to the grid
# e: Element index
# xi: The xi coordinate to be evaluated
# eta: The eta coordinate to be evaluated
# Returns: Real-space coordinate (x,y) at parameter-space
#          coordinate (xi,eta) for element e.
function ParamToReal(self::Grid2D,e,xi,eta)
    x = 0
    y = 0
    basisf = BasisParamSpace(self.p,self.q,xi,eta)
    n_element_nodes = length(basisf)
    for a = 1:n_element_nodes
        x += basisf[a] * self.nodes[1,self.IEN[a,e]]
        y += basisf[a] * self.nodes[2,self.IEN[a,e]]
    end
    return x,y
end


# ----- RealToElementParam (1D) -----
# Returns the element index, and xi coordinate
#   for a given x coordinate
# self: Pointer to the grid
# x: Real-space x coordinate
function RealToElementParam(self::Grid1D,x)
    # We assume all elements are the same size, same
    # polynomial order, and that the problem is 1D
    element_width = (M/self.m)
    e = convert(Int64,floor((x-self.Mmin)/element_width)) + 1
    xi = 2.0*(x - (e-1)*element_width)/element_width - 1.0
    return e,xi
end


# ----- RealToElementParam (2D) -----
# Returns the element index, and xi,eta coordinate
#   for a given x,y coordinate
# self: Pointer to the grid
# x: Real-space x coordinate
# y: Real-space y coordinate
function RealToElementParam(self::Grid2D,x,y)
    return 0 #TODO
end


# ----- ElementDelBasisIntegral (1D) -----
# self: pointer to the grid
# e: Element index
# Returns: Array of dimension p+1, containing the values of the integrals under
# each derivative-of-basis-function of element e, over the domain of the element
function ElementDelBasisIntegral(self::Grid1D,e)
    f(params) = DelBasisParamSpace(self.p,params[1])
    a = self.nodes[self.IEN[1,e]]
    b = self.nodes[self.IEN[self.nnodesperel,e]]
    jacobianf(params) = (b-a)/2
    return GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
end


# ----- updateD (1D) -----
# Assumes that the most recent displacements
# are in "d_vec", and sets the corresponding
# values in "d" to the values in "d_vec".
function updateD(self::Grid1D)
    for e = 1:self.nelements
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                glob_dof_i = ElementDofLookup(self,e,a,dofi)
                if(glob_dof_i > 0)
                    self.d[dofi,self.IEN[a,e]] = self.d_vec[glob_dof_i]
                end
            end
        end
    end
end


# ----- Jacobian -----
# self: Pointer to the grid
# e: Element index
# xi:
# eta:
# Returns: 2x2 matrix containing [[dx/dxi, dx/deta]; [dy/dxi, dy/deta]]
# Returns: The result of "DelBasisParamSpace" at (xi,eta), in case the caller needs it.
function Jacobian(self::Grid2D,e,xi,eta)
    J = zeros(2,2)
    delbasisf = DelBasisParamSpace(self.p,self.q,xi,eta)
    for j = 1:2
        for i = 1:2
            J[i,j] = 0
            for a = 1:self.nnodesperel
                partJ = delbasisf[j,a] * self.nodes[i,self.IEN[a,e]]
                J[i,j] += partJ
            end
        end
    end
    return J,delbasisf
end


# ----- DelBasisRealSpace (1D) -----
# self: Pointer to the grid
# e: Element index
# xi: The xi coordinate to be evaluated
# Returns:  Array, length (p+1); contains dN/dx for each basis function,
#           evaluated at the xi coordinate
function DelBasisRealSpace(self::Grid1D,e,xi)
    DELBASISparam = DelBasisParamSpace(self.p,xi)
    param_width = 2.0
    real_width = self.M/self.nelements
    DELBASISreal = DELBASISparam*(param_width/real_width)
    return DELBASISreal
end


# ----- DelBasisRealSpace (2D) -----
# self: Pointer to the grid
# e: Element index
# xi: The xi coordinate to be evaluated
# eta: The eta coordinate to be evaluated
# Returns:  Matrix, dimension 2-by-((p+1)*(q+1)); First row contains dN/dx for each basis function,
#           The second row contains dN/dy for each basis function, evaluated at the (xi,eta) coordinate
function DelBasisRealSpace(self::Grid2D,e,xi,eta)
    J,DNDXI_DNDETA = Jacobian(self,e,xi,eta)
    DNDX_DNDY = zeros(size(DNDXI_DNDETA))
    num_basisf = length(DNDX_DNDY[1,:])
    INVJ = inv(J)
    for a = 1:num_basisf
        DNDX_DNDY[:,a] = DNDXI_DNDETA[:,a]'*INVJ
    end
    return DNDX_DNDY
end


# ----- GetB -----
# self: Pointer to the grid
# e: Element index
# xi: The xi coordinate to be evaluated
# eta: The eta coodinate to be evaluated
# Returns: Matrix, dimension 3-by-2-by-((p+1)*(q+1)).
#          Each layer B[:,:,k] contains the B matrix for basis function k=a for basis function a.
function GetB(self::Grid2D,e,xi,eta)
    delbasis = DelBasisRealSpace(self,e,xi,eta)
    B = zeros(3,2,size(delbasis)[2])
    B[1,1,:] = B[3,2,:] = delbasis[1,:]
    B[2,2,:] = B[3,1,:] = delbasis[2,:]
    return B
end


# ----- ElementIntegral -----
# self: Pointer to the grid
# e: Element index
# Returns: Matrix of dimension ((p+1)*(q+1))-by-1 containing values of the integral under
# each basis function of element e over the domain of the element.
function ElementIntegral(self::Grid2D,e)
    f(params) = BasisParamSpace(self.p,self.q,params[1],params[2])
    jacobianf(params) = det(Jacobian(self,e,params[1],params[2])[1])
    return GaussQuadIntegrate(f,self.GQT2D,jacobianf)
end


# ----- ElementFaceIntegrals
# self: Pointer to the grid
# e: Element index
# Returns: Matrix of dimension ((p+1)*(q+1))-by-4; each row is for each basis basis function of the element.
#          The first column is along face xi = -1
#          The second column is along face xi = 1
#          The third column is along face eta = -1
#          The fourth column is along face eta = 1
function ElementFaceIntegrals(self::Grid2D,e)
    ANS = zeros((self.p+1)*(self.q+1),4)

    #xi=-1
    f(params) = BasisParamSpace(self.p,self.q,  -1.,  params[1])
    jacobianf(params) = norm(Jacobian(self,e,   -1,   params[1])[1][:,2])
    ANS[:,1] = GaussQuadIntegrate(f,self.GQT1Dq,jacobianf)

    #xi=1
    f(params) = BasisParamSpace(self.p,self.q,   1.,  params[1])
    jacobianf(params) = norm(Jacobian(self,e,    1.,  params[1])[1][:,2])
    ANS[:,2] = GaussQuadIntegrate(f,self.GQT1Dq,jacobianf)

    #eta=-1
    f(params) = BasisParamSpace(self.p,self.q,   params[1], -1.)
    jacobianf(params) = norm(Jacobian(self,e,    params[1], -1.)[1][:,1])
    ANS[:,3] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)

    #eta=1
    f(params) = BasisParamSpace(self.p,self.q,   params[1],  1.)
    jacobianf(params) = norm(Jacobian(self,e,    params[1],  1.)[1][:,1])
    ANS[:,4] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)

    return ANS
end


# ----- AssembleElementK -----
# self: Pointer to the grid
# e: The element index
# Returns: The element K matrix K_e for element e.
function AssembleElementK(self::Grid2D,e)

    #Allocate the K_e matrix.
    K_e = zeros(self.nnodesperel*2,self.nnodesperel*2)

    # The function evaluated in the integral
    f = function(params)
            xi = params[1]
            eta = params[2]
            B = GetB(self,e,xi,eta)
            D = self.D
            BtDB = [B[:,:,a]'*D*B[:,:,b] for a = 1:self.nnodesperel, b = 1:self.nnodesperel]
            return BtDB
        end

    #println("The jacobian function evaluated in the integral...")
    # The jacobian function evaluated in the integral
    jacobianf = function(params)
                    jac = Jacobian(self,e,params[1],params[2])[1]
                    return det(jac)
                end

    #println("Perform the integral: Integrate(B_a'*D*B_b,element_domain)...")
    # Perform the integral: Integrate(B_a'*D*B_b,element_domain)
    # (The result contains all combinations of a,b in a grid.)
    K_e_Integral_result = GaussQuadIntegrate(f,self.GQT2D,jacobianf)

    #println("Adding the parts of the K_e matrix into the K_e matrix...")
    # Adding the parts of the K_e matrix into the K_e matrix
    for a = 1:self.nnodesperel
        for b = 1:self.nnodesperel
            amin = 2(a-1)+1
            amax = 2(a-1)+2
            bmin = 2(b-1)+1
            bmax = 2(b-1)+2
            K_e[amin:amax,bmin:bmax] += K_e_Integral_result[a,b]
        end
    end

    return K_e
end


# ----- processBoundaryConditions (1D) -----
# This function performs the following:
#   (1) Assembles the "ID" array, which contains a table which maps
#       the nodal index to its corresponding dof indices.
#   (2) Determines the number of degrees of freedom.
#   (3) Sets the constrained nodal displacements, and zeros-out the rest.
# self: Pointer to the grid
# Returns: Reference to the ID array, the number of dof, and the nodal d array
function processBoundaryConditions(self::Grid1D)

    # --------------------------------------
    # Building the ID array, Setting BC info
    # --------------------------------------

    # Step 1.1: Init the ID array, set all values to 1
    # Step 1.2: Also, init the solution vector d
    self.ID = ones(Int64,2,self.nnodes)
    fill!(self.d,0.0)

    # Step 2.1: Mark the constrained dof by looping over elements,
    #         and settting the ID array to 0 for constrained elements
    # Step 2.2: Also, init the displacement BC in the final displacement vector d.
    for e = 1:self.nelements
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                hasDisplacement,d_val = self.dirichletConstraint(self,e,a,dofi)
                if( hasDisplacement )
                    self.ID[dofi,self.IEN[a,e]] = 0
                    self.d[dofi,self.IEN[a,e]] = d_val
                end
            end
        end
    end

    # Step 3: Specify the dof index for each non-constrained dof.
    dof_count = 0
    for node = 1:self.nnodes
        for dofi = 1:self.ndofpernode
            if( self.ID[dofi,node] == 1 )
                dof_count += 1
                self.ID[dofi,node] = dof_count
            end
        end
    end

    # Step 4: Set the number of degrees of freedom
    self.dof = dof_count

    return self.ID,self.dof,self.d

end


# ----- processBoundaryConditions (2D) -----
# This function performs the following:
#   (1) Assembles the "ID" array, which contains a table which maps
#       the nodal index to its corresponding dof indices.
#   (2) Determines the number of degrees of freedom.
#   (3) Sets the constrained nodal displacements, and zeros-out the rest.
# self: Pointer to the grid
# Returns: Reference to the ID array, the number of dof, and the nodal d array
function processBoundaryConditions(self::Grid2D)

    # ----------------------
    # Building the ID array
    # ----------------------

    # Step 1.1: Init the ID array, set all values to 1
    # Step 1.2: Also, init the solution vector d
    self.ID = ones(Int64,2,self.nnodes)
    fill!(self.d,0.0)

    # Step 2.1: Mark the constrained dof by looping over elements,
    #         and settting the ID array to 0 for constrained elements
    # Step 2.2: Also, init the displacement BC in the final displacement vector d.
    for e = 1:self.nelements
        for a = 1:self.nnodesperel
            for dofi = 1:2
                hasDisplacement,d_val = self.displacementConstraint(self,e,a,dofi)
                if( hasDisplacement )
                    self.ID[dofi,self.IEN[a,e]] = 0
                    self.d[dofi,self.IEN[a,e]] = d_val
                end
            end
        end
    end

    # Step 3: Specify the dof index for each non-constrained dof.
    dof_count = 0
    for node = 1:self.nnodes
        for dofi = 1:2
            if( self.ID[dofi,node] == 1 )
                dof_count += 1
                self.ID[dofi,node] = dof_count
            end
        end
    end

    # Step 4: Set the number of degrees of freedom
    self.dof = dof_count

    return self.ID,self.dof,self.d
end


# ----- ElementDisplacementVector -----
# Returns an array size (2*(p+1)*(q+1))-by-1 containing
# the current displacement on the nodes of element e.
function ElementDisplacementVector(self::Grid2D,e)
    d0_e = zeros(self.nnodesperel*2,1)
    for i = 1:self.nnodesperel
        for dofi=1:2
            d0_e[2*(i-1)+dofi] = self.d[dofi,self.IEN[i,e]]
        end
    end
    return d0_e
end


# ----- ComputeStress -----
# Computes the stress at (xi,eta) in element e.
# Returns: 3-by-1 vector, with sigma_xx, sigma_xy, and sigma_xy+sigma_yx.
function ComputeStress(self::Grid2D,e,xi,eta)
    strain = zeros(3,1)
    stress = zeros(3,1)
    d_e = reshape(ElementDisplacementVector(self,e),2,self.nnodesperel)
    B = GetB(self,e,xi,eta)
    for a = 1:self.nnodesperel
        strain += B[:,:,a]*d_e[:,a]
    end
    stress = self.D*strain
    return stress
end


# ----- assembleGlobalVector (1D) -----
# self: pointer to the grid
# VecForElement: A function with signature (self::Grid1D,e)
#                for element "e" and grid "self"
function assembleGlobalVector(self::Grid1D,vector,VecForElement::Function)
    # Assumes the "vector" array already exists

    # Initialize the residual array
    fill!(vector,0.0)

    # Loop over each element
    for e = 1:self.nelements
        # (1) Call "VecForElement"
        # (2) Add the result into the global value.
        vec_el = VecForElement(self,e)
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                glob_dof_i = ElementDofLookup(self::Grid1D,e,a,dofi)
                if(glob_dof_i > 0)
                    vector[glob_dof_i] += vec_el[dofi,a]
                end
            end
        end
    end

    return vector
end


# ----- assembleGlobalFext (1D) -----
# self: pointer to the grid
function assembleGlobalFext(self::Grid1D)
    # Assumes the "Fext" array already exists
    # Initialize the residual array
    fill!(self.Fext,0.0)
    assembleGlobalVector(self,self.Fext,self.FextForElement)
    return self.Fext
end



# ----- assembleGlobalN (1D) -----
# self: pointer to the grid
function assembleGlobalN(self::Grid1D)
    # Assumes the "Nvec" array already exists
    # Initialize the residual array
    fill!(self.Nvec,0.0)
    assembleGlobalVector(self,self.Nvec,self.NForElement)
    return self.Nvec
end



# ----- assembleGlobalResidual (1D) -----
# self: pointer to the grid
function assembleGlobalResidual(self::Grid1D)
    # Assumes the "R" array already exists

    # Initialize the residual array
    fill!(self.R,0.0)

    assembleGlobalFext(self)
    assembleGlobalN(self)

    # Use a fraction of the actual Fext, depending on the load-step
    loadloopmult = 1.0
    if(self.nloadsteps > 0)
        loadloopmult = (self.load_step_i/self.nloadsteps)
    end

    self.R = self.Fext*loadloopmult - self.Nvec

    return self.R
end



# ----- assembleGlobalConsistentTangent (1D) -----
function assembleGlobalConsistentTangent(self::Grid1D)
    # Assumes the "K" matrix already exists

    # Initialize the K matrix
    fill!(self.K,0.0)

    # Loop over each of element (pair of elements??)
    for e = 1:self.nelements
        K_el = self.consistentTangentForElement(self,e)
        for ai = 1:self.nnodesperel
            for aj = 1:self.nnodesperel
                for nd_dof_i = 1:self.ndofpernode
                    for nd_dof_j = 1:self.ndofpernode
                        k_i = ElementDofLookup(self,e,ai,nd_dof_i)
                        k_j = ElementDofLookup(self,e,aj,nd_dof_j)
                        if(k_i > 0 && k_j > 0)
                            self.K[k_i,k_j] += K_el[ai,aj][nd_dof_i,nd_dof_j]
                        end
                    end
                end
            end
        end
    end

    return self.K
end




# ----- AssembleGlobalKAndFint (2D) -----
# Assembles the global K matrix
#   NOTE: Must call "ProcessDisplacementBC" before calling this function.
# self: Pointer to the grid
# Returns: Reference to the K matrix, Fint array
function AssembleGlobalKAndFint(self::Grid2D)

    # ----------------------
    # Building the K matrix
    # ----------------------

    # Step 1: Init the K, Fint arrays
    self.K = zeros(self.dof,self.dof)
    self.Fint = zeros(self.dof)

    # For each element, assemble K_e; contribute K_e to K, and use K_e to compute the element Fint_e
    for e = 1:self.nelements

        # Assemble element K matrix
        K_e = AssembleElementK(self,e)

        # Assemble the element displacement vector
        d0_e = ElementDisplacementVector(self,e)

        # Compute the Fint_e vector
        Fint_e = K_e*d0_e

        # Contribute this element to the global K matrix, and to the global Kint array
        for a = 1:self.nnodesperel
            for dim_a = 1:2
                dof_a = ElementDofLookup(self,e,a,dim_a)
                if(dof_a > 0)
                    el_dof_a = 2(a-1)+dim_a
                    self.Fint[dof_a] += Fint_e[el_dof_a]
                    for b = 1:self.nnodesperel
                        for dim_b = 1:2
                            dof_b = ElementDofLookup(self,e,b,dim_b)
                            if( dof_b > 0 )
                                el_dof_b = 2(b-1)+dim_b
                                self.K[dof_a,dof_b] += K_e[el_dof_a,el_dof_b]
                            end
                        end
                    end
                end
            end
        end

    end

    return self.K, self.Fint
end


# ----- AssembleElementFext (2D) -----
# Assembles the external force vector for element e, and the specified side
# self: Pointer to the grid
# e: Element index
# side: The side of the element: 1: xi = -1, 2: xi = 1, 3: eta = -1, 4: eta = 1
function AssembleElementFext(self::Grid2D,e,side)
    Fext_e = zeros(self.nnodesperel*2,1)
    T = self.tractionForElement(self,e)
    FaceIntegrals = ElementFaceIntegrals(self,e)[:,side]
    reshape(Fext_e,2,self.nnodesperel)[1,:] = FaceIntegrals[:] .* T[1,:][:]
    reshape(Fext_e,2,self.nnodesperel)[2,:] = FaceIntegrals[:] .* T[2,:][:]
    return Fext_e
end


# ----- AssembleFext (2D) -----
# Assembles the external forces that result from the tractions
# self: Pointer to the grid
# Returns: reference to the Fext vector
function AssembleFext(self::Grid2D)

    self.Fext = zeros(self.dof)

    # For readability
    num_e_x = m
    num_e_y = n
    cor_botleft = 1
    cor_botright = num_e_x
    cor_topleft = (num_e_y-1)*num_e_x+1
    cor_topright = num_e_x*num_e_y

    # Integrate over left edge
    index_start = cor_botleft
    index_end = cor_topleft
    stride = num_e_x
    for e = index_start:stride:index_end
        # Integrate over the left edge of the element
        Fext_e = AssembleElementFext(self,e,1)

        # Contribute the result to the global external force vector
        for a = 1:self.nnodesperel
            for dim_a = 1:2
                dof_a = ElementDofLookup(self,e,a,dim_a)
                if(dof_a > 0)
                    el_dof_a = 2(a-1)+dim_a
                    self.Fext[dof_a] += Fext_e[el_dof_a]
                end
            end
        end
    end

    # Integrate over right edge
    index_start = cor_botright
    index_end = cor_topright
    stride = num_e_x
    for e = index_start:stride:index_end
        # Integrate over the right edge of the element
        Fext_e = AssembleElementFext(self,e,2)

        # Contribute the result to the global external force vector
        for a = 1:self.nnodesperel
            for dim_a = 1:2
                dof_a = ElementDofLookup(self,e,a,dim_a)
                if(dof_a > 0)
                    el_dof_a = 2(a-1)+dim_a
                    self.Fext[dof_a] += Fext_e[el_dof_a]
                end
            end
        end
    end

    # Integrate over bottom edge
    index_start = cor_botleft
    index_end = cor_botright
    stride = 1
    for e = index_start:stride:index_end
        #Integrate over bottom edge of the element
        Fext_e = AssembleElementFext(self,e,3)

        # Contribute the result to the global external force vector
        for a = 1:self.nnodesperel
            for dim_a = 1:2
                dof_a = ElementDofLookup(self,e,a,dim_a)
                if(dof_a > 0)
                    el_dof_a = 2(a-1)+dim_a
                    self.Fext[dof_a] += Fext_e[el_dof_a]
                end
            end
        end
    end

    # Integrate over top edge
    index_start = cor_topleft
    index_end = cor_topright
    stride = 1
    for e = index_start:stride:index_end
        #Integrate over the top edge of the element
        Fext_e = AssembleElementFext(self,e,4)

        # Contribute the result to the global external force vector
        for a = 1:self.nnodesperel
            for dim_a = 1:2
                dof_a = ElementDofLookup(self,e,a,dim_a)
                if(dof_a > 0)
                    el_dof_a = 2(a-1)+dim_a
                    self.Fext[dof_a] += Fext_e[el_dof_a]
                end
            end
        end
    end

    return self.Fext
end



# ----- AssembleFbf (2D) -----
# Assembles the body force vector that result from the body forces.
# self: Pointer to the grid
# Returns: reference to the Fbf vector
function AssembleFbf(self::Grid2D)
    self.Fbf = zeros(self.dof)
    # TODO.  This vector is zero for now.
    return self.Fbf
end



# ----- Solve (1D) -----
# Solve the FE system using Newton-Raphson
function Solve(self::Grid1D)

    # Zeroing-out the "d" array
    fill!(self.d,0.0)

    # Process the Boundary Conditions, determining global number of dof
    # Also, sets the values of self.d on boundaries.
    processBoundaryConditions(self)

    # Finish initializing all the other important arrays
    self.d_vec = zeros(self.dof)
    self.del_d = zeros(self.dof)
    self.R = zeros(self.dof)
    self.K = zeros(self.dof,self.dof)
    self.Nvec = zeros(self.dof)
    self.Fext = zeros(self.dof)
    MAX_corrloops = self.max_correction_loops

    # Load-step loop
    self.load_step_i = 0
    while(self.load_step_i <= self.nloadsteps)

        println("Load step: ",self.load_step_i," out of ",self.nloadsteps)

        #Copy the most recent 'd_vec' values into 'd'.
        updateD(self)

        # Initialize the residual (R)
        assembleGlobalResidual(self)
        println("     Correction step: ","0", "   R:     ",maximum(abs(self.R)))

        # Correction loop.
        corrloop_k = 1
        while(true)

            # Assemble the global consistent tangent (K)
            assembleGlobalConsistentTangent(self)

            #println("K:\n",self.K,"\n"); println("self.dof:",self.dof);

            self.del_d[:] = self.K\self.R     # run = rise/(rise/run)
            self.d_vec[:] += self.del_d[:]    # Update displacement guess

            # Copy the most recent 'd_vec' values into 'd'.
            updateD(self)

            # Update the residual
            #self.old_R[:] = R[:]  #"old_R" used for convergence tests.
            assembleGlobalResidual(self)

            println("     Correction step: ",corrloop_k, "   R:     ",maximum(abs(self.R)))

            # Break out of loop if residual is small enough.
            if(maximum(abs(self.R)) <= self.epsilon || corrloop_k >= MAX_corrloops)
                break
            end

            corrloop_k += 1
        end
        self.load_step_i += 1
    end

    println("Solve done!")

end


# ----- Solve (2D) -----
# Solve the FE system
# (1) Guess d0
#     (a) Determine the number of dof, set BC, and build the ID array
#     (b) Init the d_vec to the initial guess
# (2) Solve for the Internal forces Fint = K*d0
#     (a) Build the K matrix
#     (b) Solve Fint = K*d0
# (3) Assemble Fext, Fbc, and Determine R
#     (a) Assemble Fext
#     (b) Assemble Fbf (body force)
#     (c) Determine R = Fbf + Fext - Fint
# (4) Solve for delta_d, s.t. K*delta_d = R
#     (a) delta_d = K\R (matrix solve)
# (5) Obtain the final solution d_final = d0 + delta_d
#     (a) d_final = d0 + delta_d
#     (b) Set the solution values into the nodal displacement vector
#
# self: Pointer to the grid
function Solve(self::Grid2D)

    # -----------------------------------------------------------------
    # 1 a: Determine the number of dof, set BC, and build the ID array
    # -----------------------------------------------------------------

    println("Processing Displacement BC...")
    processBoundaryConditions(grid)

    # ----------------------------------------------
    # 1 b: Initing the solution displacement vector
    #      (subset of the nodal displacements array)
    # ----------------------------------------------

    println("Initing solution vector...")
    self.d_vec = zeros(self.dof)

    # -------------------------------------------------
    # 2 a, 2 b: Assemble the global K matrix, and Fint
    # -------------------------------------------------

    println("Assembling Global K and Fint...")
    AssembleGlobalKAndFint(self)

    # --------------------------
    # 3 a: Assemble Fext
    # --------------------------

    println("Assembling Fext...")
    AssembleFext(self)

    # -------------------------------
    # 3 b: Assemble Fbf (body force)
    # -------------------------------

    println("Assembling Fbf...")
    AssembleFbf(self)

    # --------------------------------------
    # 3 c: Determine R = Fbf + Fext - Fint
    # --------------------------------------

    println("Computing R...")
    R = self.Fbf + self.Fext - self.Fint

    # ----------------------------------------------------
    # 4 a: Solve for delta_d, s.t. K*delta_d = R
    # ----------------------------------------------------

    println("Solving the system delta_d = K\\R...")
    delta_d = self.K\R

    # -------------------------------------------------------
    # 5 a: Obtain the final solution d_final = d0 + delta_d
    # -------------------------------------------------------

    println("Computing new d_vec...")
    self.d_vec = self.d_vec + delta_d

    # -----------------------------------------------------------------
    # 5 b: Set the solution values into the nodal displacement vector
    # -----------------------------------------------------------------

    println("Saving solution in full d vector...")
    for i = 1:self.nnodes
        for dofi = 1:2
            if(self.ID[dofi,i] != 0)
                self.d[dofi,i] = self.d_vec[self.ID[dofi,i]]
            end
        end
    end

    println("Done.")
    return true
end



# ----- getValue (1D) -----
# Returns the specified value in element e at (xi,eta).
# self: Pointer to the grid
# value_name: The value to be returned
# e: The element index
# xi: The xi coordinate
# eta: The eta coordinate
function GetValue(self::Grid1D,value_name::ASCIIString,e,xi)
    val = 0
    x = ParamToReal(self,e,xi)
    if(value_name == "temperature")
        dTemp = zeros(self.nnodesperel)
        for a = 1:self.nnodesperel
            dTemp[a] = self.d[self.IEN[a,e]]
        end
        val = FieldValue(dTemp,self.p,xi)
    elseif( value_name == "ones" )
        val = FieldValue(ones(self.nnodesperel),self.p,xi)
    end
    return val,x
end



# ----- getValue (2D) -----
# Returns the specified value in element e at (xi,eta).
# self: Pointer to the grid
# value_name: The value to be returned
# e: The element index
# xi: The xi coordinate
# eta: The eta coordinate
function GetValue(self::Grid2D,value_name::ASCIIString,e,xi,eta)
    val = 0
    x,y = ParamToReal(self,e,xi,eta)
    if(value_name == "d_x")
        d_e_x = reshape(ElementDisplacementVector(self,e),2,self.nnodesperel)[1,:]
        val = FieldValue(d_e_x,self.p,self.q,xi,eta)
    elseif(value_name == "d_y")
        d_e_y = reshape(ElementDisplacementVector(self,e),2,self.nnodesperel)[2,:]
        val = FieldValue(d_e_y,self.p,self.q,xi,eta)
    elseif( value_name == "s_xx" )
        val = ComputeStress(self,e,xi,eta)[1]
    elseif( value_name == "s_yy" )
        val = ComputeStress(self,e,xi,eta)[2]
    elseif( value_name == "s_xy" )
        val = ComputeStress(self,e,xi,eta)[3] / 2
    elseif( value_name == "ones" )
        val = FieldValue(ones(self.nnodesperel),self.p,self.q,xi,eta)
    end
    return val,x,y
end



# ----- Plot (2D) -----
# self: Pointer to the grid
# ppe_xi: Number of points per element in the xi direction
# ppe_eta: Number of points per element in the eta direction
# Returns: Matrix, dimension (m*ppe_xi)-by-(n*ppe_eta), containing
#          points eveluated over the domain in each element.
function Plot(self::Grid2D, value_name::ASCIIString, ppe_xi, ppe_eta)
    X = zeros(self.m*ppe_xi,self.n*ppe_eta)
    Y = zeros(self.m*ppe_xi,self.n*ppe_eta)
    Z = zeros(self.m*ppe_xi,self.n*ppe_eta)
    for e_j = 1:self.n
        for e_i = 1:self.m
            e = IndexForRowCol(e_i,e_j,self.m)
            xi_points = collect(linspace(-1,1,ppe_xi+1))[1:end-1]
            eta_points = collect(linspace(-1,1,ppe_eta+1))[1:end-1]

            for pt_i = 1:ppe_xi
                for pt_j = 1:ppe_eta
                    val,x,y = GetValue(self,value_name,e,xi_points[pt_i],eta_points[pt_j])
                    X[(e_i-1)*ppe_xi + pt_i, (e_j-1)*ppe_eta + pt_j] = x
                    Y[(e_i-1)*ppe_xi + pt_i, (e_j-1)*ppe_eta + pt_j] = y
                    Z[(e_i-1)*ppe_xi + pt_i, (e_j-1)*ppe_eta + pt_j] = val
                end
            end
        end
    end
    return X,Y,Z
end
